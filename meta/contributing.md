---
tags: 
    - meta
    - contributing
---
# Contributing

These docs, like everything related to Slipmat code, live on [Slipmat GitLab](https://gitlab.com/slipmatio). You need to [register a free GitLab account](https://gitlab.com/users/sign_up) in order to submit changes and new content. The same account can also be used to submit and follow tickets, and help with working on Slipmat code and other docs.

## How To Submit Content And Corrections

Every page on this documentation site has a "Edit this page"-link at the bottom of the page. After you've created your [GitLab account](https://gitlab.com/users/sign_up), just click that link, make edits and submit them as a merge reguest (the GitLab edit page guides you, you just need to click a button).

## Technical Stuff

- [VuePress](https://vuepress.vuejs.org/) is the software that powers the docs