---
tags: 
    - development
    - isssues
    - roadmap
---

# Slipmat Development Process

Slipmat is developed with a very open process. The [developer documentation](https://developer.slipmat.io/) explains everything in great detail but for most users there's justa couple of important things to know and follow: how to report issues and the roadmap.


::: tip NOTE
Slipmat is a community project. That means **everyone in the Slipmat community can help and affect the development process.**
:::

## How To Report Issues

Everything related to Slipmat code lives on [Slipmat GitLab](https://gitlab.com/slipmatio). You need to [register a free GitLab account](https://gitlab.com/users/sign_up) in order to report and follow issues.

1. Before submitting an issue, [search the open issues list](https://gitlab.com/slipmatio/development/-/issues)
2. If your issue or feature request isn't alredy submitted, [submit a new issue](https://gitlab.com/slipmatio/development/-/issues/new)

## Roadmap

The roadmap is a longer term plan of upcoming features and development. Things on this roadmap **will** change during the initial beta phase. See the details from attached GitLab ticket links.

| Stage        | Description           | Timetable  | Details  | Status  |
| ------------ |-----------------------| -----------| --------| --------|
| Alpha 0      | Basic project and server structure | Q1/2021 | [#41](https://gitlab.com/slipmatio/development/-/issues/41) | **Done** |
| Alpha 1      | Basic user functionality | Q1/2021 | [#41](https://gitlab.com/slipmatio/development/-/issues/41) |:running: **In progress** |
| Alpha 2      | Basic artist fuctionality | Q1/2021 | N/A |Planned |
| Beta 1       | Deeper user functionality      | Q1/2021 | N/A |Planned |
| Beta 2       | Deeper artist functionality      | Q2/2021 | N/A |Planned |
| v3           | Stable functionality      | Q4/2021 | N/A |Planned |
