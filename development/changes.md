---
tags: 
    - development
    - changelog
    - changes
---

# Slipmat v3 Docs Changelog

#### 2021-02-17

* Added `How To Delete Your Account` to the [account section](/account/).
#### 2021-02-16

* Added [Account section](/account/) with first iteration of `Your Data` and `How To Export Your Data`.

#### 2021-02-11

* First version